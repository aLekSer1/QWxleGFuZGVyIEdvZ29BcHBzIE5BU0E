module gitlab.com/alekser1/QWxleGFuZGVyIEdvZ29BcHBzIE5BU0E

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.8.1
	github.com/stretchr/testify v1.7.0
)
