package nasa

import (
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestGetURL(t *testing.T) {
	// Invalid URL used
	n := NewNasaAPI("https://example.com", "d")
	res := n.GetURL(time.Now())
	assert.True(t, strings.HasPrefix(res, "json.Unmarshal error:"))
}
