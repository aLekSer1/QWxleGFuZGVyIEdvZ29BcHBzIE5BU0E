package nasa

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

// URLGetter - interface to be used to query pictures by date
type URLGetter interface {
	GetURL(time time.Time) string
}

// Nasa - parameters to make queries to NASA API
type Nasa struct {
	url     string
	demoKey string
}

// Picture is a struct for holding the NASA API response
type Picture struct {
	Date        string `json:"date"`
	Title       string `json:"title"`
	Explanation string `json:"explanation"`
	HDurl       string `json:"hdurl"`
}

// NewNasaAPI - create object of Nasa API
func NewNasaAPI(url string, demoKey string) *Nasa {
	return &Nasa{url: url, demoKey: demoKey}
}

// GetURL - get URL from NASA
func (n Nasa) GetURL(t time.Time) string {
	day := t.Format("2006-01-02")
	client := &http.Client{}
	url := fmt.Sprintf("%s?api_key=%s&date=%s", n.url, n.demoKey, day)
	req, err := http.NewRequestWithContext(context.Background(), "GET", url, nil)
	if err != nil {
		log.Printf("%s", err)
		return "error in Get"
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("%s", err)
		return "error in Do"
	}
	defer func() {
		err2 := resp.Body.Close()
		if err2 != nil {
			log.Println(err2)
		}
	}()
	l, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return "error in ReadAll"
	}

	if resp.StatusCode != http.StatusOK {
		return "error" + string(l)
	}
	nasa := &Picture{}
	err = json.Unmarshal(l, &nasa)
	if err != nil {
		return fmt.Sprintf("json.Unmarshal error: %s ", err.Error())
	}
	return nasa.HDurl
}
