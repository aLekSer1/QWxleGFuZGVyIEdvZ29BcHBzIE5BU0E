package worker

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/alekser1/QWxleGFuZGVyIEdvZ29BcHBzIE5BU0E/pkg/nasa"
)

// Request is a chan to send output chan with a request
type Request struct {
	date   time.Time
	output chan string
}

// Response list of URLs - JSON response to API client
type Response struct {
	URLs []string `json:"urls"`
}

// ErrorResponse from NASA API or any other errors while creating json with results
type ErrorResponse struct {
	Error string `json:"error"`
}

// PicturesTimeRangeHandler - handler for collecting URLs of pictures
func (s *Server) PicturesTimeRangeHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	log.Println(w, "start: ", vars["start"])
	start := r.FormValue("start_date")
	end := r.FormValue("end_date")
	log.Println(w, "start: ", start, "end: ", end)
	const shortForm = "2006-01-02"
	sTime, err := time.Parse(shortForm, start)
	if err != nil {
		log.Println(err)
	}
	eTime, err := time.Parse(shortForm, end)
	if err != nil {
		log.Println(err)
	}
	log.Println(sTime, eTime)
	resp := &Response{URLs: make([]string, 0)}
	count := 0
	timeIterator := sTime
	output := make(chan string)
	for timeIterator.Before(eTime.AddDate(0, 0, 1)) {
		log.Println(timeIterator)
		go func(t time.Time) {
			day := t.Format("2006-01-02")
			log.Println("Cache", day)
			if s.UseCache && s.Cache[day] != "" {
				output <- s.Cache[day] // Cache hit - send URL
			} else {
				s.Dates <- Request{date: t, output: output}
			}
		}(timeIterator)
		count++
		timeIterator = timeIterator.AddDate(0, 0, 1)
	}

	for i := 0; i < count; i++ {
		res := <-output
		log.Println("received", res)
		if res != "" && !strings.HasPrefix(res, "http") {
			log.Println("error")
			er := ErrorResponse{Error: res}
			w.WriteHeader(http.StatusInternalServerError)
			b, err2 := json.Marshal(er)
			if err2 != nil {
				log.Println(err2)
				return
			}
			_, err2 = w.Write(b)
			if err != nil {
				log.Println(err2)
			}

			return
		}
		resp.URLs = append(resp.URLs, res)
	}
	log.Println(resp)
	b, err := json.Marshal(resp)
	if err != nil {
		log.Println(err)
		return
	}
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(b)
	if err != nil {
		log.Println(err)
	}
}

// Server - url-collector request handler
type Server struct {
	Getter    nasa.URLGetter
	Dates     chan Request
	SleepTime time.Duration
	UseCache  bool
	Cache     map[string]string
}

// GetURLs read dates one by one from channel and send request to API to retrieve URLs
func (s *Server) GetURLs(done <-chan struct{}) {
	for request := range s.Dates {
		log.Println(request.date)
		u := s.Getter.GetURL(request.date)
		log.Println("Got ", u)
		select {
		case request.output <- u:
		case <-done:
			return
		}
		time.Sleep(s.SleepTime)
	}
}
