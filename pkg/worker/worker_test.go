package worker

import (
	"log"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type N int

func (n N) GetURL(t time.Time) string {
	return "http://example.com/" + t.Format("2006-03-04")
}

func TestGetURLs(t *testing.T) {
	n := N(0)
	s := Server{UseCache: true, Getter: n, Dates: make(chan Request), SleepTime: time.Millisecond * time.Duration(50), Cache: make(map[string]string)}
	// For testing
	/*
		t := time.Now().Add(time.Hour * 24)
		s.Cache[t.Format("2006-01-02")] = "http://www.example.com"
	*/

	// Start a fixed number of goroutines to read and digest files.
	c := make(chan string)
	var wg sync.WaitGroup
	done := make(chan struct{})
	defer close(done)

	timeIterator := time.Now()
	eTime := timeIterator.AddDate(0, 1, 0)
	count := 0

	output := make(chan string)
	for timeIterator.Before(eTime.AddDate(0, 0, 1)) {
		log.Println(timeIterator)
		go func(t time.Time) {
			day := t.Format("2006-01-02")
			log.Println("Cache", day)
			if s.UseCache && s.Cache[day] != "" {
				output <- s.Cache[day] // Cache hit - send URL
			} else {
				s.Dates <- Request{date: t, output: output}
			}
		}(timeIterator)
		count++
		timeIterator = timeIterator.AddDate(0, 0, 1)
	}

	numDigesters := 5
	wg.Add(numDigesters)
	for i := 0; i < numDigesters; i++ {
		go func() {
			log.Println("started")
			s.GetURLs(done)
			log.Println("done")
			wg.Done()
		}()
	}
	go func() {
		wg.Wait()
		close(c)
	}()
	resp := &Response{URLs: make([]string, 0)}
	for i := 0; i < count; i++ {
		res := <-output
		log.Println("received", res)
		assert.True(t, strings.HasPrefix(res, "http"))
		resp.URLs = append(resp.URLs, res)
	}
	log.Println(resp)
	assert.Len(t, resp.URLs, count)
}
