package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestParseEnv verify default config varialbes
func TestParseEnv(t *testing.T) {
	c := parseENV()
	fmt.Println(c)
	assert.NotNil(t, c)

	assert.Equal(t, 5, c.ConcurrentRequests, "Number of concurrent requests")
	assert.Equal(t, 1000, c.RequestsSleep, "Sleep in milliseconds before new NASA API call")
}
