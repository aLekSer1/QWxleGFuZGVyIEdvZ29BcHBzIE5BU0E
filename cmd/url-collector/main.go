/*
Copyright © 2021 Alexander

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/mux"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/alekser1/QWxleGFuZGVyIEdvZ29BcHBzIE5BU0E/pkg/nasa"
	"gitlab.com/alekser1/QWxleGFuZGVyIEdvZ29BcHBzIE5BU0E/pkg/worker"
)

const (
	useCacheFlag             = "use_cache"
	apiKeyFlag               = "api_key"
	concurrentRequestsFlag   = "concurrent_requests"
	portFlag                 = "port"
	pauseBetweenRequestsFlag = "pause_duration"
)

// Config for url-collector
type Config struct {
	APIKey             string
	Port               int
	ConcurrentRequests int
	UseCache           bool
	RequestsSleep      int
}

func parseENV() Config {
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	pflag.Bool(useCacheFlag, viper.GetBool(useCacheFlag), "Use cache of a good queries")
	pflag.String(apiKeyFlag, viper.GetString(apiKeyFlag), "API_KEY environment variable for NASA API")
	pflag.Int(concurrentRequestsFlag, viper.GetInt(concurrentRequestsFlag), "Concurrent requests to NASA API")
	pflag.Int(pauseBetweenRequestsFlag, viper.GetInt(pauseBetweenRequestsFlag), "Pause between requests to NASA API, in milliseconds")
	pflag.Int(portFlag, viper.GetInt(portFlag), "Port for URL collector")
	viper.SetDefault(apiKeyFlag, "DEMO_KEY")
	viper.SetDefault(pauseBetweenRequestsFlag, 1000)
	viper.SetDefault(useCacheFlag, false)
	viper.SetDefault(concurrentRequestsFlag, 5)
	viper.SetDefault(portFlag, 8000)
	return Config{APIKey: viper.GetString(apiKeyFlag),
		Port:               viper.GetInt(portFlag),
		ConcurrentRequests: viper.GetInt(concurrentRequestsFlag),
		RequestsSleep:      viper.GetInt(pauseBetweenRequestsFlag),
	}
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	r := mux.NewRouter()
	config := parseENV()

	log.Printf("%+v\n", config)
	n := nasa.NewNasaAPI("https://api.nasa.gov/planetary/apod", "DEMO_KEY")
	log.Println(n.GetURL(time.Now().Add(time.Hour * -24)))

	log.Println("new")

	s := worker.Server{UseCache: config.UseCache, Getter: n, Dates: make(chan worker.Request), SleepTime: time.Millisecond * time.Duration(config.RequestsSleep), Cache: make(map[string]string)}

	// For testing
	/*
		t := time.Now().Add(time.Hour * 24)
		s.Cache[t.Format("2006-01-02")] = "http://www.example.com"
	*/
	r.Path("/pictures").Queries("start_date", "", "end_date", "").HandlerFunc(s.PicturesTimeRangeHandler).Name("YourHandler")

	// Start a fixed number of goroutines to read and digest files.
	c := make(chan string)
	var wg sync.WaitGroup
	done := make(chan struct{})
	defer close(done)

	numDigesters := config.ConcurrentRequests
	wg.Add(numDigesters)
	for i := 0; i < numDigesters; i++ {
		go func() {
			log.Println("started")
			s.GetURLs(done)
			log.Println("done")
			wg.Done()
		}()
	}
	go func() {
		wg.Wait()
		close(c)
	}()
	const ReadWriteTimeout = 15
	srv := &http.Server{
		Handler:      r,
		Addr:         "0.0.0.0:" + strconv.Itoa(config.Port),
		WriteTimeout: ReadWriteTimeout * time.Second,
		ReadTimeout:  ReadWriteTimeout * time.Second,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	sig := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(sig, os.Interrupt)

	// Block until we receive our signal.
	<-sig

	// Create a deadline to wait for.
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	srv.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	log.Println("shutting down")
	os.Exit(0)
}

// nolint: godox
// TODO list:
//  - add bufferised chan to RateLimit the number of concurrent requests to the NASA API
//  - add port to dockerfile
//  - think about local caching of previous results - we can use the cache to avoid requesting the same URL twice
//  - handle errors  just return the error to the user
//  - add query validations
//  - refactor
//  - add unit tests
//  - some integration tests with mocks
