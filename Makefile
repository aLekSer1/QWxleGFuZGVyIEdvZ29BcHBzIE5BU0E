
base_version = 1.0
VERSION ?= $(base_version)-$(shell git rev-parse --short=7 HEAD)
IMAGE_NAME=url-collector:$(VERSION)

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
build_path := $(dir $(mkfile_path))

build-image:
	docker build -f ./cmd/url-collector/Dockerfile -t $(IMAGE_NAME) .

run-image:
	docker run -d -p 8080:8080  -e PORT=8080 -e USE_CACHE=true $(IMAGE_NAME)

lint:
	docker run --rm -v $(build_path):/app -w /app  golangci/golangci-lint:v1.40.1 golangci-lint run
